
#ifndef CALCULATRICE_PARSER_H
#define CALCULATRICE_PARSER_H

#include <iostream>
#include <string>
#include <sstream>



#include "Expression/include/Expression.h"

class Parser {
private:
	Parser();

	static const std::string *src;
    static int index;

    static char last_char;
    static int last_cst;

    static bool read_char(char c);
    static bool read_cst();
    static Expression* read_e();
    static Expression* read_e_mul();
    static Expression* read_e_unary();
    static Expression* read_e_cst();
    static void error();
public:
    static Expression* parse_on(const std::string* txt);
};


#endif //CALCULATRICE_PARSER_H
