#include "Parser.h"
#include "Expression/BinOpps/include/Add.h"
#include "Expression/BinOpps/include/Sub.h"
#include "Expression/BinOpps/include/Div.h"
#include "Expression/BinOpps/include/Mul.h"
#include "Expression/UnaryOpps/include/Neg.h"
#include "Expression/UnaryOpps/include/Neutral.h"
#include "Expression/include/Cst.h"

int Parser::index = 0;
char Parser::last_char;
int Parser::last_cst;
const std::string *Parser::src;

bool Parser::read_char(char c) {
	if ((index < src->length()) && (src->at(index) == c)) {
		index++;
		last_char = c;
		return true;
	}
	return false;
}

bool Parser::read_cst() {
	bool flagcst = false;
	bool flag = false;
	std::stringstream stream;
	do {
		flag = false;
		for (int i = 0; i < 10; ++i) {
			if (read_char((char) (i + '0'))) {
				stream << last_char;
				flag = true;
				flagcst = true;
			}
		}
	} while (flag);
	last_cst = std::stoi(stream.str());
	return flagcst;
}

Expression *Parser::parse_on(const std::string *txt) {
	Expression *e;
	src = txt;
	index = 0;

	e = read_e();
	if ((e == nullptr) || (index < src->length())) error();
	return e;
}

Expression *Parser::read_e() {
	Expression *result, *right;
	char op;
	result = read_e_mul();
	if (result != nullptr) {
		while ((read_char('+') || read_char('-'))) {
			op = last_char;
			right = read_e_mul();
			if (right == nullptr) error();
			if (op == '+')
				result = new Add(result, right);
			else
				result = new Sub(result, right);
		}
	}
	return result;
}

void Parser::error() {
	int j;
	std::cout << src << std::endl;
	for (int i = 0; i < index; ++i) {
		std::cout << ' ';
	}
	std::cout << 'I' << std::endl;
	exit(EXIT_FAILURE);
}

Expression *Parser::read_e_mul() {
	Expression *result, *right;
	char op;
	result = read_e_unary();
	if (result != nullptr) {
		while ((read_char('*') || read_char('/'))) {
			op = last_char;
			right = read_e_unary();
			if (right == nullptr) error();
			if (op == '*')
				result = new Mul(result, right);
			else
				result = new Div(result, right);
		}
	}
	return result;
}

Expression *Parser::read_e_unary() {
	Expression *result;

	if (read_char('-')) {
		result = read_e_cst();
		return new Neg(result);
	} else {
		result = read_e_cst();
		return new Neutral(result);
	}

}

Expression *Parser::read_e_cst() {
	Expression *expression = nullptr;
	if (read_char('(')) {
		expression = read_e();
		if (!read_char(')'))
			error();
	} else {
		if (read_cst())
			expression = new Cst(last_cst);
	}

	return expression;
}

Parser::Parser() = default;
