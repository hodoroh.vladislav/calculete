
#include "include/Div.h"

Div::Div(Expression *l, Expression *r) : ExprBinary(l,r) {

}

int Div::eval() {
    return this->right->eval() == 0 ?
        0 :
        this->left->eval() / this->right->eval();
}
