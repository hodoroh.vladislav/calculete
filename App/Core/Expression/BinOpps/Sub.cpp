
#include "include/Sub.h"

Sub::Sub(Expression *l, Expression *r) : ExprBinary(l, r) {

}

int Sub::eval() {
    return this->left->eval() - this->right->eval();
}
