
#ifndef CALCULATRICE_ADD_H
#define CALCULATRICE_ADD_H


#include "../../include/ExprBinary.h"

class Add : public ExprBinary {

public:
    Add(Expression* l, Expression* r);
    int eval() override;

};

#endif //CALCULATRICE_ADD_H
