
#ifndef CALCULATRICE_DIV_H
#define CALCULATRICE_DIV_H


#include "../../include/Expression.h"
#include "../../include/ExprBinary.h"

class Div : public ExprBinary {
public:
    Div(Expression* l, Expression* r);
    int eval() override;
};


#endif //CALCULATRICE_DIV_H
