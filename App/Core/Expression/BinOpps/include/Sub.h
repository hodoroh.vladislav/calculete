
#ifndef CALCULATRICE_SUB_H
#define CALCULATRICE_SUB_H


#include "../../include/ExprBinary.h"

class Sub : public ExprBinary {

public:
    Sub(Expression* l, Expression* r);
    int eval() override;

};


#endif //CALCULATRICE_SUB_H
