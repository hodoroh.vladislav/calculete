
#ifndef CALCULATRICE_MUL_H
#define CALCULATRICE_MUL_H


#include "../../include/ExprBinary.h"

class Mul : public ExprBinary {
public:
    Mul(Expression* l, Expression* r);
    int eval() override;
};


#endif //CALCULATRICE_MUL_H
