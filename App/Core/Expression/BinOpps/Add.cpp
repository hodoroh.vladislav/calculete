
#include "include/Add.h"

Add::Add(Expression *l, Expression* r) : ExprBinary(l,r)
{

}

int Add::eval() {
    return this->left->eval() + this->right->eval();
}
