
#include "include/Mul.h"

Mul::Mul(Expression *l, Expression *r) : ExprBinary(l, r) {

}

int Mul::eval() {
    return this->left->eval() * this->right->eval();
}
