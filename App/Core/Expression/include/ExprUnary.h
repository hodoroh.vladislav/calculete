
#ifndef CALCULATRICE_EXPRUNARY_H
#define CALCULATRICE_EXPRUNARY_H


#include "Expression.h"

class ExprUnary : public  Expression {
public:
    Expression* left;

    ExprUnary(Expression* l);
};


#endif //CALCULATRICE_EXPRUNARY_H
