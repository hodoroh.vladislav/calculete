
#ifndef CALCULATRICE_EXPRBINARY_H
#define CALCULATRICE_EXPRBINARY_H


#include "Expression.h"

class ExprBinary : public Expression {

public:
    ExprBinary(Expression* l, Expression* r);

    Expression* right;
    Expression* left;
};


#endif //CALCULATRICE_EXPRBINARY_H
