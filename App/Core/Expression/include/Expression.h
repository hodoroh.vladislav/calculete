
#ifndef CALCULATRICE_EXPRESSION_H
#define CALCULATRICE_EXPRESSION_H


class Expression {
public:
    virtual int eval() = 0;
};


#endif //CALCULATRICE_EXPRESSION_H
