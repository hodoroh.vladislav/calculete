
#ifndef CALCULATRICE_CST_H
#define CALCULATRICE_CST_H


#include "Expression.h"

class Cst : public  Expression {
public:
    int value;

    explicit Cst(int value);

    int eval() override{
        return value;
    }
};


#endif //CALCULATRICE_CST_H
