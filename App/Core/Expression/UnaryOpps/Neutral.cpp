
#include "include/Neutral.h"

Neutral::Neutral(Expression *left) : ExprUnary(left) {

}

int Neutral::eval() {
    return left->eval();
}
