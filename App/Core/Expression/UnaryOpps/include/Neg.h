
#ifndef CALCULATRICE_NEG_H
#define CALCULATRICE_NEG_H


#include "../../include/ExprUnary.h"

class Neg : public ExprUnary{
    int eval() override;

public:
    explicit Neg(Expression* expr);
};


#endif //CALCULATRICE_NEG_H
