
#ifndef CALCULATRICE_NEUTRAL_H
#define CALCULATRICE_NEUTRAL_H


#include "../../include/ExprUnary.h"

class Neutral : public ExprUnary {
public:
    explicit Neutral(Expression *left);
    int eval() override;
};


#endif //CALCULATRICE_NEUTRAL_H
