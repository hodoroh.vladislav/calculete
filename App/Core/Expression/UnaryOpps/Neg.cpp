
#include "include/Neg.h"

Neg::Neg(Expression *expr) : ExprUnary(expr) {

}

int Neg::eval() {
    return this->left->eval() * (-1);
}


