#include <iostream>
#include "App/Core/Parser.h"


int main() {
    std::string string = "2+3-15-1";
    std::cout << Parser::parse_on(&string)->eval();
    return 0;
}
