cmake_minimum_required(VERSION 3.15)
project(Calculatrice)

set(CMAKE_CXX_STANDARD 20)
file(GLOB_RECURSE app RECURSE ${CMAKE_SOURCE_DIR}/App/*.cpp)

#add_subdirectory(App/Core)
link_directories(App)

add_executable(Calculatrice main.cpp ${app})